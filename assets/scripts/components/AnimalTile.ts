// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import GameManager, { MatrixData } from "../GameManager";
import AudioManager from "../utils/AudioManager";
import { AuidoKey } from "../utils/Const";

const {ccclass, property} = cc._decorator;

@ccclass
export default class AnimalTile extends cc.Component {

    private _matrixData: MatrixData;
    public get matrixData(): MatrixData {
        return this._matrixData;
    }
    public set matrixData(value: MatrixData) {
        this._matrixData = value;
    }

    @property(cc.Material)
    grayMaterial: cc.Material;
    @property(cc.Material)
    normalMaterial: cc.Material;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
    }

    onClick() {
        // this.node.getComponent(cc.AudioSource).play();
        AudioManager.instance.play(AuidoKey.CLICK);
        GameManager.instance.setData(this._matrixData);
    }

    gray() {
        this.node.children[0].getComponent(cc.Sprite).setMaterial(0, this.grayMaterial);
    }

    normal() {
        this.node.children[0].getComponent(cc.Sprite).setMaterial(0, this.normalMaterial);
    }

    // update (dt) {}
}
