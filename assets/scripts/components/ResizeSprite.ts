// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html
const {ccclass, property, requireComponent} = cc._decorator;
@ccclass
@requireComponent(cc.Sprite)
export default class ResizeSprite extends cc.Component {


    @property(cc.Node)
    parentNode: cc.Node;

    private _scaleFactor: number = 0.75;
    public get scaleFactor(): number {
        return this._scaleFactor;
    }
    @property({type: cc.Float})
    public set scaleFactor(value: number) {
        this._scaleFactor = value;
        this._resize();
    }

    private _currentSF : cc.SpriteFrame;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    private _resize() {
        if (!this.node.getComponent(cc.Sprite)) return;
        if (!this.node.getComponent(cc.Sprite).spriteFrame) return;
        this._currentSF = this.node.getComponent(cc.Sprite).spriteFrame;
        let size = this._currentSF.getRect().clone().size;
        let bound = this.parentNode.getContentSize().clone();
        let ratio = size.width / size.height;
        if (ratio >= 1) {
            size.width = bound.width * this.scaleFactor;
            size.height = size.width / ratio;
        }
        else {
            size.height = bound.height * this.scaleFactor;
            size.width = size.height * ratio;
        }
        this.node.setContentSize(size);
    }

    update (dt : number) {
        if (this._currentSF !== this.node.getComponent(cc.Sprite)!.spriteFrame) {
            this._resize();
        }
    }
}
