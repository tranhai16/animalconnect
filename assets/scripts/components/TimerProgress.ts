// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "../GameManager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class TimerProgress extends cc.Component {

    private _counters: number = 0;
    private _maxTime: number = 0;
    private _isRunning: boolean = false;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    initData(maxTime: number) {
        this._maxTime = maxTime;
    }

    run() {
        this._counters = 0;
        this._isRunning = true;
    }

    stop() {
        this._isRunning = false;
    }
    start () {

    }

    update (dt: number) {
        if (this._isRunning) {
            this._counters += dt;
            if (this._counters >= this._maxTime) {
                GameManager.instance.timeEnded();
                this._isRunning = false;
            } else {
                const progress = this._counters / this._maxTime;
                this.node.getComponent(cc.ProgressBar).progress = progress;
            }
        }
    }
}
