// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import GameManager, { MatrixData, UIData } from "./GameManager";
import AnimalTile from "./components/AnimalTile";
import AudioManager from "./utils/AudioManager";
import { AuidoKey } from "./utils/Const";
import { GameUtils, ShowEffect } from "./utils/GameUtils";
import LevelConfig from "./utils/LevelConfig";
import MainView from "./views/MainView";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Main extends cc.Component {
    // @property(cc.AudioSource)
    // exellentSource: cc.AudioSource;
    // @property(cc.AudioSource)
    // wrongSource: cc.AudioSource;

    mainView: MainView;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        AudioManager.instance.init(this.node);
        AudioManager.instance.addAudio(AuidoKey.CLICK, 'raws/touch');
        AudioManager.instance.addAudio(AuidoKey.EXELLENT, 'raws/exellent');
        AudioManager.instance.addAudio(AuidoKey.WRONG, 'raws/wrong');
        AudioManager.instance.mute = true;
        Promise.all([
            GameUtils.loadLevelConfig(),
            GameUtils.loadAnimalSprites(),
            GameUtils.loadPrefab('prefabs/animalTile'),
            GameUtils.loadPrefabToNode('prefabs/main', this.node, ShowEffect.FROM_BOTTOM),
        ]).then((value : [any, cc.SpriteFrame[], cc.Prefab, cc.Node]) => {
            GameManager.instance.levelConfigs = [];
            (value[0] as any).forEach((v : any) => {
                GameManager.instance.levelConfigs.push(new LevelConfig().parseData(v));
            })
            GameManager.instance.spriteFrameData = value[1] as cc.SpriteFrame[];
            GameManager.instance.initAnimalPrefab(value[2]);
            this.mainView = value[3].getComponent(MainView);
            this.mainView.reloadUI();
            // this.node.addChild(value[2]);
        }).catch((error : Error) => { console.log(error); });
    }

    protected onEnable(): void {
        GameManager.instance.eventTarget.on('new-game', this.addNewGame, this);
    }
    protected onDisable(): void {
        GameManager.instance.eventTarget.off('new-game', this.addNewGame, this);
    }
    addNewGame(): void {
        this.mainView && this.mainView.reloadUI();
    }
    start () {
    }

    // update (dt) {}
}
