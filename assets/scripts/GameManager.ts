import AnimalTile from "./components/AnimalTile";
import Algorithm from "./utils/Algorithm";
import { GameUtils } from "./utils/GameUtils";
import LevelConfig from "./utils/LevelConfig";
import { generateLevels } from "./utils/LevelGenerate";
import Line from "./utils/Line";

const eventTarget: cc.EventTarget = new cc.EventTarget();

export default class GameManager {
    

    private static _instance: GameManager;
    private mapData: Map<MatrixData, UIData> = new Map();
    private animalPrefab: cc.Prefab;
    private _currentLevel: number = 1;
    private _currentLevelTime: number = 0;
    private _levelMaxTime: number = 0;
    public get eventTarget(): cc.EventTarget {
        return eventTarget;
    }
    public static get instance(): GameManager {
        if (!this._instance) this._instance = new GameManager();
        return GameManager._instance;
    }

    spriteFrameData: cc.SpriteFrame[] = [];
    levelConfigs: LevelConfig[] = [];
    currentLevelData: LevelConfig;
    private constructor() {
    }

    initAnimalPrefab(prefab: cc.Prefab) {
        this.animalPrefab = prefab;
    }

    private _animalConnectData: MatrixData[] = [];

    public setData(data: MatrixData) {
        if (this._animalConnectData.length === 1 && this._animalConnectData[0].equals(data)) {
            this._animalConnectData = [];
            this.mapData.get(data).node.getComponent(AnimalTile).gray();
            return;
        };
        this.mapData.get(data).node.getComponent(AnimalTile).normal();
        this._animalConnectData.push(data);
        if (this._animalConnectData.length === 2) {
            this.check();
        }
    }
    check() {
        const startData = this._animalConnectData[0];
        const endData = this._animalConnectData[1];
        const startUIData = this.mapData.get(startData);
        const endUIData = this.mapData.get(endData);
        if (startUIData.animalIndex === endUIData.animalIndex) {
            let path = Algorithm.instance.canConnect(startData.toVec2(), endData.toVec2());
            if (path) {
                console.log('Animals connected!');
                let linePoints: cc.Vec2[] = [];
                let startPosition = this.mapData.get(startData).uiPosition;
                let contentSize = this.mapData.get(startData).node.getContentSize();
                path.forEach((point) => {
                    let normalized = point.clone().subtract(startData.toVec2());
                    let linePoint = new cc.Vec2(startPosition.x + normalized.x * contentSize.width, startPosition.y + normalized.y * contentSize.height);
                    linePoints.push(linePoint);
                });

                eventTarget.emit('draw-line', linePoints);
                setTimeout(() => {
                    Algorithm.instance.removes([startData.toVec2(), endData.toVec2()]);
                    eventTarget.emit('play-exellent');
                    this.updateUI();
                    this._animalConnectData = [];
                }, 200);
            }
            else {
                this._animalConnectData = [];
                eventTarget.emit('play-wrong');
                eventTarget.emit('updateUI');
            }

            return;

        }
        console.log('Animals not connected yet');

        this._animalConnectData = [];
        eventTarget.emit('play-wrong');
        eventTarget.emit('updateUI');

    }
    timeEnded() {
        if (this.mapData.size > 0) {
            console.log('Game over!');
            eventTarget.emit('game-over');
            return;
        }
        console.log('Congratulations! You won the game!');
        this._currentLevel++;
        eventTarget.emit('new-game');
    }

    public async initGame(): Promise<void> {
        if (!this.animalPrefab) return;
        // console.log('Generate Level', JSON.stringify(generateLevels(100)))
        // console.log('Level configuration', this.levelConfigs);
        // Implement your game initialization logic here
        this.currentLevelData = this.levelConfigs.find(data => data.level === this._currentLevel);
        let rows = this.currentLevelData.rows;
        let cols = this.currentLevelData.cols;
        // let countType = this.currentLevelData.max_tiles_gen;
        // let tiles = rows * cols / countType;
        Algorithm.instance.generate(rows, cols, this.currentLevelData.array_tiles.length, this.currentLevelData.array_tiles);
        Algorithm.instance.setGoCenter(this.currentLevelData.go_center_horizontal, this.currentLevelData.go_center_vertical);
        this.updateUI();
    }

    private updateUI() {
        this.mapData.clear();
        // let suggest = Algorithm.instance.suggest;
        for (let i = 0; i < Algorithm.instance.matrix.length; i++) {
            for (let j = 0; j < Algorithm.instance.matrix[i].length; j++) {
                let matrixData: MatrixData = new MatrixData(i, j);
                
                const spriteFrame = this.spriteFrameData.find(sf => sf.name === (Algorithm.instance.matrix[i][j]).toString());
                if (spriteFrame) {
                    const node = cc.instantiate(this.animalPrefab);
                    node.children[0].getComponentInChildren(cc.Sprite).spriteFrame = spriteFrame;
                    let size = node.getContentSize();
                    let anchor = node.getAnchorPoint();
                    let nextPosition = cc.v3((i + anchor.x - Algorithm.instance.rows / 2) * size.width,
                        (j + anchor.y - Algorithm.instance.cols / 2) * size.height);
                    // cc.tween(node).to(0.1, {position: nextPosition}).start();
                    node.setPosition(nextPosition);
                    node.getComponent(AnimalTile).matrixData = matrixData;
                    // if (suggest.length > 0 && (suggest[0].start.equals(matrixData.toVec2()) || suggest[0].end.equals(matrixData.toVec2()))) {
                    //     node.getComponent(AnimalTile).normal();
                    // }
                    let ui: UIData = new UIData(node, Algorithm.instance.matrix[i][j]);
                    this.mapData.set(matrixData, ui);
                }
            }
        }
        if (this.mapData.size === 0) {
            this.timeEnded();
        }
        eventTarget.emit('refreshUI', this.mapData);
    }
    public startGame(): void {
        // Implement your game start logic here
    }

}

export class MatrixData {
    col: number;
    row: number;
    constructor(row?: number, col?: number) {
        this.col = col;
        this.row = row;
    }

    equals(other: MatrixData): boolean {
        return this.col === other.col && this.row === other.row;
    }

    toVec2(): cc.Vec2 {
        return cc.v2(this.row, this.col);
    }
}

export class UIData {
    animalIndex: number;
    node: cc.Node;
    constructor(node?: cc.Node, animalIndex?: number) {
        this.node = node;
        this.animalIndex = animalIndex;
    }

    public get uiPosition(): cc.Vec2 {
        return this.node.getPosition();
    }
}


