export default class Line {
    private _start: cc.Vec2;
    public get start(): cc.Vec2 {
        return this._start;
    }
    public set start(value: cc.Vec2) {
        this._start = value;
    }
    private _end: cc.Vec2;
    public get end(): cc.Vec2 {
        return this._end;
    }
    public set end(value: cc.Vec2) {
        this._end = value;
    }
    constructor(start: cc.Vec2 = new cc.Vec2(), end: cc.Vec2 = new cc.Vec2()) {
        this._start = start;
        this._end = end;
    }
}