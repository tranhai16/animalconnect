import Line from "./Line";

export default class Algorithm {
    remove(point: cc.Vec2) {
        this._matrix[point.x][point.y] = -1;

    }
    removes(point: cc.Vec2[]) {
        point.forEach((p) => {
            this.remove(p);
        });
        if (this._goCenterHorizontal) {
            this._matrix = this.moveElementsToCenterHorizontally(this._matrix)
        }
        if (this._goCenterVertical) {
            this._matrix = this.moveElementsToCenterVertically(this._matrix)
        }
        this.findSuggest();
        if (this._suggest.length === 0) {
            console.log('Not found a suggestion');
            this.shuffleMatrix(this._matrix);
        }
    }

    private findSuggest() {
        let data: cc.Vec2[] = [];
        for (let i = 0; i < this._rows; i++) {
            for (let j = 0; j < this._cols; j++) {
                if (this._matrix[i][j] !== -1) {
                    data.push(cc.v2(i, j));
                }
            }
        }
        this._suggest = [];
        for (let i = 0; i < data.length - 1; i++) {
            for (let j = i + 1; j < data.length; j++) {
                if (this._matrix[data[i].x][data[i].y] === this._matrix[data[j].x][data[j].y]
                    && this.canConnect(data[i], data[j])) {
                    this._suggest.push({ start: data[i], end: data[j]});
                    return;
                }
            }
        }
    }
    shuffleMatrix(_matrix: number[][]) {
        for (let i = 0; i < _matrix.length; i++) {
            for (let j = 0; j < _matrix[i].length; j++) {
                const temp = _matrix[i][j];
                const randomIndex = Math.floor(Math.random() * _matrix[i].length);
                _matrix[i][j] = _matrix[i][randomIndex];
                _matrix[i][randomIndex] = temp;
            }
        }
        this.findSuggest();
    }
    private _matrix: number[][] = [];
    public get matrix(): number[][] {
        return this._matrix;
    }
    private _cols: number;
    public get cols(): number {
        return this._cols;
    }
    private _rows: number;
    public get rows(): number {
        return this._rows;
    }

    private _suggest: { start: cc.Vec2; end: cc.Vec2; }[] = [];
    public get suggest(): { start: cc.Vec2; end: cc.Vec2; }[] {
        return this._suggest;
    }
    private static _instance: Algorithm;
    public static get instance(): Algorithm {
        if (!this._instance) {
            this._instance = new Algorithm();
        }
        return Algorithm._instance;
    }

    private constructor() {
    }
    private _goCenterHorizontal: boolean = false;
    private _goCenterVertical: boolean = false;

    setGoCenter(go_center_horizontal: boolean, go_center_vertical: boolean) {
        this._goCenterHorizontal = go_center_horizontal;
        this._goCenterVertical = go_center_vertical;
    }

    generate(n_rows: number, n_columns: number, n_types: number, count: number[]) {
        this._rows = n_rows;
        this._cols = n_columns;
        this._matrix = Array.from({ length: n_rows }, () => Array(n_columns).fill(-1));;
        const countType: Map<number, number> = new Map();
        for (let type = 0; type < n_types; ++type) {
            countType.set(type, 0);
        }

        for (let i = 0; i < this._rows; ++i) {
            for (let j = 0; j < this._cols; ++j) {
                let type: number;
                do {
                    type = Math.floor(Math.random() * n_types);
                } while (countType.get(type)! >= count[type]);

                countType.set(type, countType.get(type)! + 1);
                this._matrix[i][j] = type;
            }
        }
    }

    private printArr() {
        let str = '';
        for (let i = 0; i < this._matrix.length; i++) {
            for (let j = 0; j < this._matrix[i].length; j++) {
                str += (this._matrix[i][j] < 10 ? ('0' + this._matrix[i][j]) : this._matrix[i][j]) + '\t';
            }
            str += '\n';
        }
        console.log(str);
    }

    private findPath(_x: number, _y: number, x: number, y: number): Pair[] {
        // INIT Graph
        const e: number[][] = Array.from({ length: this._rows + 2 }, () => Array(this._cols + 2).fill(0));
        for (let i = 0; i < this._rows; ++i) {
            for (let j = 0; j < this._cols; ++j) {
                e[i + 1][j + 1] = this._matrix[i][j] !== -1 ? 1 : 0;
            }
        }
        const s: Pair = [_x + 1, _y + 1];
        const t: Pair = [x + 1, y + 1];

        // BFS
        const dx: number[] = [-1, 0, 1, 0];
        const dy: number[] = [0, 1, 0, -1];
        const q: Pair[] = [];
        const trace: Pair[][] = Array.from({ length: e.length }, () => Array.from({ length: e[0].length }, () => [-1, -1] as Pair));
        q.push(t);
        trace[t[0]][t[1]] = [-2, -2];
        e[s[0]][s[1]] = 0;
        e[t[0]][t[1]] = 0;

        while (q.length > 0) {
            const u: Pair = q.shift()!;
            if (u[0] === s[0] && u[1] === s[1]) break;
            for (let i = 0; i < 4; ++i) {
                let nx = u[0] + dx[i];
                let ny = u[1] + dy[i];
                while (nx >= 0 && nx < e.length && ny >= 0 && ny < e[0].length && e[nx][ny] === 0) {
                    if (trace[nx][ny][0] === -1) {
                        trace[nx][ny] = u;
                        q.push([nx, ny]);
                    }
                    nx += dx[i];
                    ny += dy[i];
                }
            }
        }

        // Trace back
        const res: Pair[] = [];
        if (trace[s[0]][s[1]][0] !== -1) {
            while (s[0] !== -2) {
                res.push([s[0] - 1, s[1] - 1]);
                const [nx, ny] = trace[s[0]][s[1]];
                s[0] = nx;
                s[1] = ny;
            }
        }
        return res;
    }

    canConnect(pointStart: cc.Vec2, pointEnd: cc.Vec2): cc.Vec2[] | null {
        let path = this.findPath(pointStart.x, pointStart.y, pointEnd.x, pointEnd.y);
        if (path.length >= 2 && path.length <= 4) {
            return path.map((p) => { return cc.v2(p[0], p[1]) });
        }
        else return null;
    }

    private moveElementsToCenterHorizontally(matrix: number[][]): number[][] {
        const rows = matrix.length;
        const cols = matrix[0].length;

        // Initialize a new matrix with -1
        const newMatrix = Array.from({ length: rows }, () => Array(cols).fill(-1));

        for (let i = 0; i < rows; i++) {
            const rowElements = [];

            // Gather all non -1 elements in the current row
            for (let j = 0; j < cols; j++) {
                if (matrix[i][j] !== -1) {
                    rowElements.push(matrix[i][j]);
                }
            }

            // Calculate the start position to center the elements horizontally
            const start = Math.floor((cols - rowElements.length) / 2);

            // Place the elements in the new matrix row, centered horizontally
            for (let j = 0; j < rowElements.length; j++) {
                newMatrix[i][start + j] = rowElements[j];
            }
        }

        return newMatrix;
    }

    private moveElementsToCenterVertically(matrix: number[][]): number[][] {
        const rows = matrix.length;
        const cols = matrix[0].length;

        // Initialize a new matrix with -1
        const newMatrix = Array.from({ length: rows }, () => Array(cols).fill(-1));

        for (let j = 0; j < cols; j++) {
            const colElements = [];

            // Gather all non -1 elements in the current column
            for (let i = 0; i < rows; i++) {
                if (matrix[i][j] !== -1) {
                    colElements.push(matrix[i][j]);
                }
            }

            // Calculate the start position to center the elements vertically
            const start = Math.floor((rows - colElements.length) / 2);

            // Place the elements in the new matrix column, centered vertically
            for (let i = 0; i < colElements.length; i++) {
                newMatrix[start + i][j] = colElements[i];
            }
        }

        return newMatrix;
    }
}

export type Pair = [number, number];




