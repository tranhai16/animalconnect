export default class AudioManager {
    
    
    private static _instance: AudioManager;
    public static get instance(): AudioManager {
        if (!AudioManager._instance) {
            AudioManager._instance = new AudioManager();
        }
        return AudioManager._instance;
    }

    private _root: cc.Node = null;
    private _audioSources: Map<string, cc.AudioSource>;
    private _mute: boolean = false;
    public get mute(): boolean {
        return this._mute;
    }
    public set mute(value: boolean) {
        this._mute = value;
        if (this._mute) {
            this.stopAll();
        }
        else {
            this.resumeAll();
        }
    }
    private constructor() {
        this._audioSources = new Map<string, cc.AudioSource>();
    }

    init(root: cc.Node): void {
        this._root = root;
    }

    addAudio(key: string, audioClip: string | cc.AudioClip) : void {
        if (!this._root) {
            console.error("Root node not found. Please set the root node by init method before adding audio clips.");
            return;
        }
        if (this._audioSources.has(key)) {
            console.warn(`Audio clip with key ${key} already exists.`);
            return;
        }
        if (audioClip instanceof cc.AudioClip) {
            let node = new cc.Node(audioClip.name);
            node.addComponent(cc.AudioSource);
            node.getComponent(cc.AudioSource).clip = audioClip;
            this._root.addChild(node);
            this._audioSources.set(key, node.getComponent(cc.AudioSource));
            console.log(`Added audio clip ${audioClip.name} to the audio manager.`);
        }
        else {
            cc.resources.load(audioClip, cc.AudioClip, (err, ac) => {
                if (err) {
                    console.error(`Failed to load audio clip ${audioClip} from ${audioClip}.`, err);
                    return;
                }
                let node = new cc.Node(ac.name);
                node.addComponent(cc.AudioSource);
                node.getComponent(cc.AudioSource).clip = ac;
                this._root.addChild(node);
                this._audioSources.set(key, node.getComponent(cc.AudioSource));
                console.log(`Added audio clip ${ac.name} to the audio manager.`);
            });
        }
        
    }

    play(key: string, loop: boolean = false) {
        if (this._mute) return;
        if (this._audioSources.has(key)) {
            this._audioSources.get(key).play();
            this._audioSources.get(key).loop = loop;
        } else {
            console.error(`Audio clip with key ${key} not found.`);
        }
    }
    
    stop(key: string) {
        if (this._audioSources.has(key)) {
            this._audioSources.get(key).stop();
        } else {
            console.error(`Audio clip with key ${key} not found.`);
        }
    }

    resumeAll() {
        this._audioSources.forEach((audioSource, key) => {
            if (audioSource.loop) {
                audioSource.play();
            }
        });
    }

    stopAll() {
        this._audioSources.forEach((audioSource, key) => {
            this.stop(key);
        });
    }
}