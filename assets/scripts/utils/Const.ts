export enum AuidoKey {
    BACKGROUND = 'background',
    EFFECT = 'effect',
    MUSIC = 'music',
    SOUND_EFFECT = 'soundEffect',
    UI_SOUND = 'uiSound',
    VOICE = 'voice',
    CLICK = 'click',
    EXELLENT = 'exellent',
    WRONG = 'wrong',
}