const { ccclass, property } = cc._decorator;

export enum ShowEffect {
    OPACITY,
    FROM_BOTTOM,
    FROM_TOP,
}

export class GameUtils {
    static getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static getRandomFloat(min: number, max: number): number {
        return Math.random() * (max - min) + min;
    }

    static shuffleArray(array: any[]): void {
        for (let i = array.length - 1; i >= 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    static formatNumberWithCommas(num: number): string {
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    static formatTime(time: number): string {
        const minutes = Math.floor(time / 60);
        const seconds = Math.floor(time % 60);
        return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    }

    static formatDistance(distance: number): string {
        const meters = Math.floor(distance);
        const centimeters = Math.floor((distance - meters) * 100);
        return `${meters}m ${centimeters}cm`;
    }

    static loadAnimalSprites(): Promise<cc.SpriteFrame[]> {
        return new Promise<cc.SpriteFrame[]>((resolve, reject) => {
            console.log("Loading resources...");
            cc.resources.loadDir('textures/foods/', cc.SpriteFrame, (err, data) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    console.log("Resource load failed");
                    return;
                }
                resolve(data);
                console.log("Resources loaded.");
            })
        });
    }
    static loadLevelConfig(): Promise<any> {

        return new Promise((resolve, reject) => {
            console.log("Loading level config...");
            cc.loader.loadRes('data/LevelConfig', cc.JsonAsset, (err, data) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load level config: ${err}`);
                    return;
                }
                resolve(data.json);
                console.log("Level config loaded.");
            })
        })
    }

    static loadPrefab(path: string): Promise<cc.Prefab> {
        return new Promise<cc.Prefab>((resolve, reject) => {
            console.log(`Loading prefab: ${path}`);
            cc.resources.load(path, cc.Prefab, (err, prefab) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load prefab: ${path}, ${err}`);
                    return;
                }
                resolve(prefab);
                console.log(`Prefab loaded: ${path}`);
            })
        })
    }
    static loadPrefabToNode(path: string, parent: cc.Node, showEffect: ShowEffect = ShowEffect.OPACITY): Promise<cc.Node> {
        return new Promise<cc.Node>((resolve, reject) => {
            console.log(`Loading prefab: ${path}`);
            cc.resources.load(path, cc.Prefab, (err, prefab) => {
                if (err) {
                    reject(err);
                    console.error(`Failed to load prefab: ${path}, ${err}`);
                    return;
                }
                const node = cc.instantiate(prefab);
                parent.addChild(node);
                switch (showEffect) {

                    case ShowEffect.FROM_TOP:
                        node.setPosition(0, parent.height);
                        cc.tween(node).to(0.2, { y: 0 }, {}).call(() => {
                            resolve(node);
                        }).start();
                        break;
                    case ShowEffect.FROM_BOTTOM:
                        node.setPosition(0, -parent.height);
                        cc.tween(node).to(0.2, { y: 0 }, {}).call(() => {
                            resolve(node);
                        }).start();
                        break;
                    case ShowEffect.OPACITY:
                    default:
                        node.opacity = 0;
                        cc.tween(node).to(0.2, { opacity: 255 }, {}).call(() => {
                            resolve(node);
                        }).start();
                        break;

                }
                console.log(`Prefab loaded: ${path}`);
            })
        })
    }
}


