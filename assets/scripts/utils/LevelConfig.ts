export default class LevelConfig {
    private _level: number;
    public get level(): number {
        return this._level;
    }
    public set level(value: number) {
        this._level = value;
    }
    private _rows: number;
    public get rows(): number {
        return this._rows;
    }
    public set rows(value: number) {
        this._rows = value;
    }
    private _cols: number;
    public get cols(): number {
        return this._cols;
    }
    public set cols(value: number) {
        this._cols = value;
    }
    // private _max_tiles_gen: number;
    // public get max_tiles_gen(): number {
    //     return this._max_tiles_gen;
    // }
    // public set max_tiles_gen(value: number) {
    //     this._max_tiles_gen = value;
    // }
    private _array_tiles: number[];
    public get array_tiles(): number[] {
        return this._array_tiles;
    }
    public set array_tiles(value: number[]) {
        this._array_tiles = value;
    }
    
    private _max_time: number;
    public get max_time(): number {
        return this._max_time;
    }
    public set max_time(value: number) {
        this._max_time = value;
    }
    private _go_center_horizontal: boolean;
    public get go_center_horizontal(): boolean {
        return this._go_center_horizontal;
    }
    public set go_center_horizontal(value: boolean) {
        this._go_center_horizontal = value;
    }
    private _go_center_vertical: boolean;
    public get go_center_vertical(): boolean {
        return this._go_center_vertical;
    }
    public set go_center_vertical(value: boolean) {
        this._go_center_vertical = value;
    }

    parseData(data: any): LevelConfig {
        this._level = data.level;
        this._rows = data.rows;
        this._cols = data.cols;
        // this._max_tiles_gen = data.max_tiles_gen;
        this._array_tiles = data.array_tiles;
        this._max_time = data.max_time;
        this._go_center_horizontal = data.go_center_horizontal;
        this._go_center_vertical = data.go_center_vertical;
        return this;
    }
}