// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import AnimalTile from "../components/AnimalTile";
import TimerProgress from "../components/TimerProgress";
import GameManager, { MatrixData, UIData } from "../GameManager";
import AudioManager from "../utils/AudioManager";
import { AuidoKey } from "../utils/Const";
import { GameUtils, ShowEffect } from "../utils/GameUtils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MainView extends cc.Component {
    
    @property(cc.Graphics)
    lineDraw: cc.Graphics;
    @property(cc.Node)
    readyNode: cc.Node;
    @property(cc.Node)
    startNode: cc.Node;

    private _timerProgress: TimerProgress;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
    }
    showBtnStart() {
        this.startNode.active = true;
    }
    protected onEnable(): void {
        GameManager.instance.eventTarget.on('refreshUI', this.refreshUI, this);
        GameManager.instance.eventTarget.on('updateUI', this.updateUI, this);
        GameManager.instance.eventTarget.on('draw-line', this.drawLine, this);
        GameManager.instance.eventTarget.on('play-exellent', this.playExellent, this);
        GameManager.instance.eventTarget.on('play-wrong', this.playWrong, this);
    }
    start () {
    }

    playGame() {
        this.readyNode.active = false;
        this._timerProgress.run();
    }

    reloadUI() {
        this.startNode.active = false;
        GameManager.instance.initGame();
        this.readyNode.active = true;
        let progressNode = this.node.getChildByName("progress");
        progressNode.removeAllChildren();
        GameUtils.loadPrefabToNode('prefabs/time_progress', progressNode, ShowEffect.FROM_TOP).then((node: cc.Node) => {
            this._timerProgress = node.getComponent(TimerProgress);
            this._timerProgress.initData(GameManager.instance.currentLevelData.max_time);
            this.showBtnStart();
        })
    }
    // update (dt) {}

    playExellent () {
        // this.exellentSource.play();
        AudioManager.instance.play(AuidoKey.EXELLENT);

    }
    playWrong () {
        // this.wrongSource.play();
        AudioManager.instance.play(AuidoKey.WRONG);
    }
    refreshUI(mapData: Map<MatrixData, UIData>) {
        const drawNode = this.node.getChildByName('drawNode');
        drawNode.removeAllChildren();
        mapData.forEach(uiData => drawNode.addChild(uiData.node));
        
    }

    updateUI() {
        const drawNode = this.node.getChildByName('drawNode');
        drawNode.children.forEach(child => {
            child.getComponent(AnimalTile).gray();
        })
    }

    drawLine(points: cc.Vec2[]) {
        this.lineDraw.clear();
        this.lineDraw.lineWidth = 5;
        // this.lineDraw.strokeColor.fromHEX('#ff0000');
        this.lineDraw.moveTo(points[0].x, points[0].y);
        for (let i = 1; i < points.length; i++) {
            this.lineDraw.lineTo(points[i].x, points[i].y);
        }
        // this.lineDraw.lineTo(0, -80);
        // this.lineDraw.lineTo(40, 0);
        // this.lineDraw.lineTo(0, 80);
        // this.lineDraw.close();
        this.lineDraw.stroke();
        setTimeout(() => {this.lineDraw.clear()}, 200);
        // this.lineDraw.fill();
    }
    protected onDisable(): void {
        GameManager.instance.eventTarget.off('refreshUI', this.refreshUI, this);
        GameManager.instance.eventTarget.off('updateUI', this.updateUI, this);
        GameManager.instance.eventTarget.off('draw-line', this.drawLine, this);
        GameManager.instance.eventTarget.off('play-exellent', this.playExellent, this);
        GameManager.instance.eventTarget.off('play-wrong', this.playWrong, this);
    }
}
